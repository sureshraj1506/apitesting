package com.assurity.library;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.given;

public class Test {

	
	
	public static void main(String[] args) throws IOException
    {
		
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        String directory = System.getProperty("user.dir")+"/src/test/resource/testdata/Testdata_AcceptanceCriteriaTest.json";
  	  
        try (FileReader reader = new FileReader(directory))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONObject jobj = (JSONObject) obj;
            JsonPath jp = new JsonPath(jobj.toString());
            String tovalidate = jp.get().toString();
            String arr[] = tovalidate.split(",");
            int criteria = 0;
            for (String string : arr) {
				if(string.contains("Acceptance_Criteria")){
					criteria++;
				}
			}
            System.out.println(jp.prettify());
            System.out.println(jp.getString("Acceptance_Criteria2"));
            
            
            
            
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
 
}

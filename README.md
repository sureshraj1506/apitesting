How to run the test
-------------------
Download/Clone the code from bitbucket using the below url

https://sureshraj1506@bitbucket.org/sureshraj1506/apitesting.git

Open any IDE(Eclipse preferred) and import as a existing maven project

If you want to run as a maven build - > right click the pom.xml and run as mvn install(Maven should be installed in clipse)

If you want to run in command Promt - > Navigate to project folder and give mvn clan install

eg: C:\Users\ProjectFolder mvn clean install

If you want to run as TestNG - > RightClick the testng xml and run as TestNG suite(TestG should be installed in Eclipse

Reporting
---------
FOr better understanding, I used Extent report for reporting

Installation
------------
Java 1.7 and latest
IDE(Eclipse)
Maven

Validation
----------
validated the all mentioned acceptamce criteria 





